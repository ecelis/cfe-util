CFE-Util
========

Recycled snippets of code Me uses in almost every Java project.

It features Data Base, Data Conversion and File System utility classes.

There are two supported branches, ona Java6 which currently is **master** in
Git and **java7** branch. cfe-util-0.x.x is for Java6 and cfe-util-1.x.x is for
the java7 branch.
