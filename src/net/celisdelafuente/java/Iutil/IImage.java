package net.celisdelafuente.java.Iutil;

public interface IImage {
	
	public int merge(int w, int h);
	public int save();
	public int toJpg(String img);
}
