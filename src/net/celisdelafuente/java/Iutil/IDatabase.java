package net.celisdelafuente.java.Iutil;

public interface IDatabase {
	
	IDatabase getDatabase();
	
	void init(String appName, String path, 
			boolean createDB);
}
