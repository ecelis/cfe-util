/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package net.celisdelafuente.java.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;

public class FileSystem {
	
	private String _appHome;
	private String _appName;
	
	public FileSystem() {
		// Default constructor
	}
	
	/**
	 * Constructor takes String app name and sets app home to $HOME/_appName
	 * by default.
	 * @param appName
	 */
	public FileSystem(String appName) {
		_appName = appName;
		setAppHome();
	}

	/**
	 * Set the application default working directory.
	 * @return TRUE if success, FALSE otherwise.
	 */
	public boolean setAppHome() {
		return setAppHome("");
	}
	
	/**
	 * Take String path for App Home, if empty a default $HOME/_appName will
	 * be set, if path does not exists it will be created.
	 * @param path
	 * @return
	 */
	public boolean setAppHome(String path) {
		// TODO add a check for a valid path
		if (!path.isEmpty()) {
			_appHome = path;
		} else {
			_appHome = System.getProperty("user.home") + File.separator
				+ _appName;
		}
		File home = new File(_appHome);
		if(!home.exists()) {
			if(home.mkdir()) {
				return true;
			} else {
				// TODO log error here
				return false;
			}
		}
		return true;
	}
	
	public String getAppHome() {
		return _appHome;
	}
	
	public void setAppName(String name) {
		_appName = name;
	}
	
	public String getAppName() {
		return _appName;
	}
	
	/**
	 * Change file names to lower case filtering by a wildcard
	 * @param path Directory where files reside
	 * @param wildcard File filter
	 * @param preserve Original file deleted after succesful rename, 
	 * default false
	 * @return True when success
	 */
	public static boolean lowerCaseFiles(String path, String wildcard,
		boolean preserve) {
		if(wildcard == null || wildcard.isEmpty())
			wildcard = "*";	
		File directory = new File(path);
		for(File file : directory.listFiles()) {
			//File source = iterator.next();
			File target = new File(path + File.separator 
				+ StringUtils.lowerCase(file.getName()));
			try {
				if(!preserve) {
					FileUtils.moveFile(file, target);
				} else {
					FileUtils.copyFile(file, target);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				//return false;
			}
		}
		return true;
	}
}
