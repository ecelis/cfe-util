/**
* Licensed to you under one or more contributor license agreements.  
* See the NOTICE file distributed with this work for additional information
* regarding copyright ownership.  
* Ernesto Angel Celis de la Fuente licenses this file to you under the 
* Apache License, Version 2.0 (the "License"); you may not use this file except
* in compliance with the License.  You may obtain a copy of the License at
*
*   http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing,
* software distributed under the License is distributed on an
* "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
* KIND, either express or implied.  See the License for the
* specific language governing permissions and limitations
* under the License.
*/

package net.celisdelafuente.java.util;

import net.celisdelafuente.java.Iutil.IDatabase;

public class DerbyDatabase implements IDatabase {
	
	private static DerbyDatabase db;
	
	private DerbyDatabase() {
		// Disallow clients from using the default constructor
	}
	// Allowed only instances
	public DerbyDatabase getDatabase() {
		if (db == null) {
			db = new DerbyDatabase();
		}
		return db;
	}
	
	public synchronized void init(String appName, String path, 
			boolean createDB) {
		FileSystem fs;
		if(path.isEmpty()) {
			fs = new FileSystem(appName);
		} else {
			fs = new FileSystem();
			fs.setAppName(appName);
			fs.setAppHome(path);
		}
		if(createDB) {
			// TODO initialize empty DB for application from some sort of template
		}
	}
}
