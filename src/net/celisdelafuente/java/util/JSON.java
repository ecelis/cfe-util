package net.celisdelafuente.java.util;

public class JSON {
	
	private final String STATUS = "'status':";
	private final String ERRMSG = "'error':";
	private final String DATA = "'data':";
	
	public String wrapJson(String jsonData, String status, String errorMsg) {
		String response =  "{";
		if(!status.isEmpty())
			response += STATUS + status + ",";
		if(!errorMsg.isEmpty())
			response += ERRMSG + "'" + errorMsg + "',";
		response += DATA + jsonData + "}";
		return response;
	}

}
